﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;

namespace FSL.WUIVSPluginRE
{
    public partial class MyControl : UserControl
    {
        private void AddMicrocoreWindow()
        {
            IntPtr hWnd = MicrocoreWindow.FindWindow("microcore", null);

            MicrocoreWindow window = new MicrocoreWindow(hWnd);

            if (window != null)
            {
                MicrocoreBridge.SendGenericMessage("Hosted");

                microcoreWindowPanel.Child = window;
            }
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            MicrocoreBridge.Settings settings;
            settings.window.width = 1000;
            settings.window.height = 600;
            settings.window.className = "microcore";

            IntPtr hInstance = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]);

            int result = MicrocoreBridge.Initialiaze(hInstance, settings);

            if (result == 0)
            {
                AddMicrocoreWindow();

                RunOnMainThread();
            }
            else
            {
                MessageBox.Show("Failed to initialize microcore, result: " + result, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnUnload(object sender, RoutedEventArgs e)
        {
            MicrocoreBridge.SendGenericMessage("Should close");

            MicrocoreBridge.CloseBrowser();

            microcoreWindowPanel.Child = null;
        }

        private void RunOnMainThread()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                MicrocoreBridge.Run();
                MicrocoreBridge.Shutdown();
            });
        }

        public MyControl()
        {
            InitializeComponent();
        }
    }
}
