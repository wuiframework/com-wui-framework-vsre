﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace FSL.WUIVSPluginRE
{
    public partial class MicrocoreWindow : HwndHost
    {
        private const int GWL_STYLE = (-16);
        private const int WS_CHILD = 0x40000000;

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private IntPtr ChildHandle = IntPtr.Zero;

        public MicrocoreWindow(IntPtr handle)
        {
            this.ChildHandle = handle;
        }

        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            HandleRef href = new HandleRef();

            if (ChildHandle != IntPtr.Zero)
            {
                SetWindowLong(this.ChildHandle, GWL_STYLE, WS_CHILD);
                SetParent(this.ChildHandle, hwndParent.Handle);
                href = new HandleRef(this, this.ChildHandle);
            }

            return href;
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            // Destruction is actually done in the "MyToolWindow::OnUnload"
        }

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
    }
}
