﻿using System;
using System.Runtime.InteropServices;

namespace FSL.WUIVSPluginRE
{
    public partial class MicrocoreBridge
    {
        // For the time being, only absolute path works, because we have no final version of the microcore.dll with could distribute along with this extension
        // (VS won't find the relative path to it, because we're using the devenv.exe process for lauching)
        private const string microcorePath = @"c:\burda\projects\wui\com-wui-framework-microcore\build\src\Release\microcore.dll";
        private const CallingConvention callingConvention = CallingConvention.Cdecl;

        public struct Settings
        {
            public struct Window
            {
                public int width;
                public int height;
                public string className;
            }

            public Window window;
        };

        [DllImport(microcorePath, CallingConvention = callingConvention, EntryPoint = "Initialize")]
        internal static extern int Initialiaze(IntPtr hInstance, Settings settings);

        [DllImport(microcorePath, CallingConvention = callingConvention, EntryPoint = "Run")]
        internal static extern void Run();

        [DllImport(microcorePath, CallingConvention = callingConvention, EntryPoint = "Shutdown")]
        internal static extern void Shutdown();

        [DllImport(microcorePath, CallingConvention = callingConvention, EntryPoint = "CloseBrowser")]
        internal static extern void CloseBrowser();

        [DllImport(microcorePath, CallingConvention = callingConvention, EntryPoint = "SendGenericMessage")]
        internal static extern void SendGenericMessage(string message);
    }
}
