﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WUIVSPluginRE")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("FSL")]
[assembly: AssemblyProduct("WUIVSPluginRE")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]   
[assembly: ComVisible(false)]     
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("WUIVSPluginRE_IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100ab52cdda388934d82828ea3f4910f9bc3acc4842a74906e4b057d166833a13104759ea6111c5edf18ccedb45ed4f4265dffd3655e615ca1348742ad036df167002849a4a19cec8218702956985c6576fcfb647cd2b1c74bb86f1968316d837f328a49aea6a5185711eb4d9d28d841a0813b0e0c3cb391b79585c25d4457a87cb")]
[assembly: InternalsVisibleTo("WUIVSPluginRE_UnitTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100ab52cdda388934d82828ea3f4910f9bc3acc4842a74906e4b057d166833a13104759ea6111c5edf18ccedb45ed4f4265dffd3655e615ca1348742ad036df167002849a4a19cec8218702956985c6576fcfb647cd2b1c74bb86f1968316d837f328a49aea6a5185711eb4d9d28d841a0813b0e0c3cb391b79585c25d4457a87cb")]
